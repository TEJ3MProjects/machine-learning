# Machine Learning

This repostiory contains all of my machine learning projects

### Cat Vs Dog Classifier

A machine learning model that can classify CIFAR cats and dogs using a convolutional neural network with 98% accuracy. 

### CatDogClassifierWebApp

A web application that can take in images of cats and dogs and classify them based on a convolutional neural network machine learning model. 

### Quantum Convolutional Neural Network

A machine learning model that can classify excitation in qubits using a quantum circuit model

### Hybrid Cats Vs Dogs Classifier

A Hybrid quantum convolutional neural network model that uses a dressed quantum net to classify CIFAR cats and dogs.

### Other files

README.md -> This files

journal.md -> Links to journals

sources.md -> Links to inspiration sources
