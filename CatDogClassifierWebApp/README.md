# Cat Dog Classifier Web App

This web app classifies cats and dogs using a convoluted neural network. 
I implemented the web app using Flask and HTML/CSS/JS with a Heroku local deployment. 

## Tools Used:
* Tensorflow/Keras
* h5/h5py
* Flask
* HTML/CSS/JS
* Bootstrap
* Heroku

## Next Steps: 

* Use Hybrid learning model
* Increase accuracy of web app
* Use Docker or Kubernetes to deploy app
* Look into other deep learning CIFAR algorithms
