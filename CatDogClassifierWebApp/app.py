# Import flask and tensorflow dependencies
import os
from flask import Flask, render_template, request, send_from_directory
from keras_preprocessing import image
from keras.models import load_model
import numpy as np
import tensorflow as tf

# Start the flask app
app = Flask(__name__)

# Update paths to the upload and model folders
STATIC_FOLDER = 'static'
UPLOAD_FOLDER = STATIC_FOLDER + '/uploads'
MODEL_FOLDER = STATIC_FOLDER + '/models'

# Load the tensorflow model
def load__model():
    global model
    model = load_model(MODEL_FOLDER + '/cat_dog_classifier.h5')
    global graph
    graph = tf.get_default_graph()

# Predict and preprocess the data
def predict(fullpath):
    data = image.load_img(fullpath, target_size=(128, 128, 3))
    # Scale the dimensions (150,150,3) ==> (1,150,150,3)
    data = np.expand_dims(data, axis=0)
    data = data.astype('float') / 255
    # Predict the data
    with graph.as_default():
        result = model.predict(data)
    return result

# Home page reroute
@app.route('/')
def index():
    return render_template('index.html')

# Process file and predict his label
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'GET':
        # Return the main html file
        return render_template('index.html')
    else:
        # Request the image file
        file = request.files['image']
        fullname = os.path.join(UPLOAD_FOLDER, file.filename)
        file.save(fullname)
        result = predict(fullname)
        pred_prob = result.item()
        # Get the prediction probability
        if pred_prob > .5:
            label = 'Dog'
            accuracy = round(pred_prob * 100, 2)
        else:
            label = 'Cat'
            accuracy = round((1 - pred_prob) * 100, 2)
        # Return the predicted label and accuracy
        return render_template('predict.html', image_file_name=file.filename, label=label, accuracy=accuracy)

# Upload file reroute
@app.route('/upload/<filename>')
def send_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)

# Create the Flask app
def create_app():
    load__model()
    return app

# Run the app on deploy
if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
